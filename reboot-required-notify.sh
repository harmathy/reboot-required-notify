#!/bin/bash
#
# Copyright (C) 2021-2024 Max Harmathy <harmathy@mailbox.org>
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <http://www.gnu.org/licenses/>.

IFS=' ' read -r -a linux_package_data < <(pacman -Q linux)

installed=${linux_package_data[1]}
running=$(uname -r)

# a x.y.0 kernel version will report the trailing '.0' on uname, however, the
# package version omits it, e.g 6.7.0-arch3-1 vs. 6.7.arch3-1
running=${running/.0-/-}

# the version string has a hyphen, where a dot is in the packages version
# e.g. 6.0.5-arch1-1 vs. 6.0.5.arch1-1
running=${running/-/.}

if ! [ "$running" = "$installed" ]
then
  dbus-send --system / net.nuetzlich.SystemNotifications.Notify \
    "string:Reboot required" \
    "string:Currently running kernel ($running) is outdated ($installed)."
fi
